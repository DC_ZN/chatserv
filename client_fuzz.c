#include "proto_inhat.h"
#include "epoll_utils.h"
#include "util.h"

#include <stdio.h>
#include <sys/socket.h>
#include <sys/epoll.h>
#include <netdb.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

#define DEFAULT_PORT 4000
#define BUF_SIZE 65536

int main(int argc, char* argv[]) {

    if (argc < 3) {
        fprintf(stderr, "Usage: %s host port\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    printf("Connecting to chat server...");
    fflush(stdout);

    struct addrinfo hints, *result;
    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_family = AF_INET;
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = 0;
    hints.ai_protocol = IPPROTO_TCP;

    int s = getaddrinfo(argv[1], argv[2], &hints, &result);
    if (s != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
        exit(EXIT_FAILURE);
    }

    // getaddrinfo() returns a list of address structures.
    // Try each address until we successfully connect(2).
    // If socket(2) (or connect(2)) fails, we (close the socket
    // and) try the next address.
    int sockfd = -1;
    struct addrinfo *rp;
    for (rp = result; rp != NULL; rp = rp->ai_next) {
        sockfd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
        if (sockfd == -1) {
            perror("socket");
            continue;
        }

        if (connect(sockfd, rp->ai_addr, rp->ai_addrlen) != -1) {
            break; // Success
        }
        //perror("connect");
        close(sockfd);
    }

    if (rp == NULL) {
        fprintf(stderr, "[error]: cannot connect to host");
        exit(EXIT_FAILURE);
    }

    printf("success.\n");

    setnonblocking(0);
    uint8_t buf[BUF_SIZE];

    ssize_t pos = 0;
    for (; pos < BUF_SIZE; ) {
        ssize_t n = read(0, buf + pos, BUF_SIZE - pos);
        if (n <= 0) {
            if (n == 0 || errno == EAGAIN || errno == EWOULDBLOCK) {
                break;
            } else {
                perror("reading");
                exit(EXIT_FAILURE);
            }
        } else {
            pos += n;
        }
    }

    for (ssize_t i = pos; i > 0; ) {
        ssize_t n = write(sockfd, buf, i);
        if (n <= 0) {
            break;
        }
        i -= n;
    }
}

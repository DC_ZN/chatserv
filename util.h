#pragma once

#include <stdint.h>
#include <stddef.h>
#include <stdio.h>

#define DEBUG 1

// Dirty hack for logging into file
#ifdef LOG_TO_FILE
extern FILE *logfile;
#define logd(fmt, ...) \
    do { if (DEBUG) fprintf(logfile, "%s:%d:%s(): " fmt "\n", __FILE__, \
                                    __LINE__, __func__, ##__VA_ARGS__); fflush(logfile);} while (0)
#endif // LOG_TO_FILE

#define logd(fmt, ...) \
    do { if (DEBUG) fprintf(stderr, "%s:%d:%s(): " fmt "\n", __FILE__, \
                                    __LINE__, __func__, ##__VA_ARGS__); fflush(stderr);} while (0)

#define logd1(str) \
    do { if (DEBUG) fprintf(stderr, "%s:%d:%s(): " str "\n", __FILE__, \
                                    __LINE__, __func__); } while (0)

void logd_hex(const char *prefix, const uint8_t *data, size_t len);

#pragma once

#include <stdint.h>
#include <stdbool.h>

typedef struct chat_broadcast {
    char* text;
    char* from;
    int refcounter;
    struct chat_broadcast* prev;
    struct chat_broadcast* next;

} chat_broadcast_t;

chat_broadcast_t* chat_broadcast_alloc(char* from, uint8_t* data, int len);
void chat_broadcast_free(chat_broadcast_t* el);

void chat_broadcast_ref(chat_broadcast_t *el);
void chat_broadcast_unref(chat_broadcast_t *el);

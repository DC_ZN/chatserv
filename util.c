#include "util.h"
#include <stdio.h>

void logd_hex(const char *prefix, const uint8_t *data, size_t len) {
    fprintf(stderr, "%s", prefix);
    for (int i = 0; i < len; ++i) {
        fprintf(stderr, " %02x", data[i]);
    }
    fprintf(stderr, "\n");
}

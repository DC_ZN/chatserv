#pragma once

#include "chat_broadcast.h"
#include "third_party/uthash/include/uthash.h"
#include <stdint.h>
#include <stddef.h>
#include <sys/epoll.h>

typedef enum {
    CL_UNKNOWN = 0,
    CL_RECOGNIZED,
    CL_LOGGED_IN,
    CL_DISCONNECTING, // write out, then disconnect
    CL_DISCONNECT
} client_state_t;

struct client_info {
    int fd;
    uint32_t requested_events;
    uint32_t active_events;

    client_state_t client_state;
    char* username;

    uint8_t* incoming_data; // buffer
    size_t incoming_len;

    uint8_t* outgoing_data; // buffer
    size_t outgoing_len;

    // non-owning pointer
    chat_broadcast_t* current_broadcast; // to track next messages

    UT_hash_handle hh;
};

// Add new client, register it with epoll
void hashtable_client_add(struct client_info** p_hashtable, int fd, uint32_t events, int epfd);

// Add new events to active_events, removing them from requested_events
// Note: without re-registering with epoll!
void hashtable_client_update_events(struct client_info** p_hashtable, const struct epoll_event* event);

void hashtable_client_schedule_disconnect(struct client_info** p_hashtable, int fd);

// Remove client, deregister with epoll, close fd
void hashtable_client_close(struct client_info** p_hashtable, int fd, int epfd);

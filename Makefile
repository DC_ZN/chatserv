CFLAGS+= -Wall -Wpedantic -std=gnu11

HEADERS=proto_inhat.h client_store.h broadcast_store.h chat_broadcast.h epoll_utils.h util.h


.PHONY: all allasan init lint

all: chatd chat-cli exploit chat-fuzz chatd-stdin

allasan: CFLAGS+=-fsanitize=address -O1 -g -Wno-gnu-zero-variadic-macro-arguments
allasan: CC:=clang
allasan: all


# use clang-tidy as default linter for server code
lint: server.c proto_inhat.c client_store.c broadcast_store.c \
	chat_broadcast.c epoll_utils.c util.c
lint: CFLAGS:=-checks=-*,clang-analyzer-*,-clang-analyzer-cplusplus*
# lint: CFLAGS:=clang-analyzer-*
lint: CC:=clang-tidy
lint:
	$(CC) $(CFLAGS) $^


chatd: server.c proto_inhat.c client_store.c broadcast_store.c \
chat_broadcast.c epoll_utils.c util.c
	$(CC) -o chatd $^ $(CFLAGS)

chat-cli: client.c proto_inhat.c util.c epoll_utils.c
	$(CC) -o chat-cli $^ $(CFLAGS)

exploit: exploit.c proto_inhat.c util.c epoll_utils.c
	$(CC) -o exploit $^ $(CFLAGS)

chat-fuzz: client_fuzz.c epoll_utils.c
	$(CC) -o chat-fuzz $^ $(CFLAGS)

chatd-stdin: server_fuzz.c proto_inhat.c client_store.c broadcast_store.c \
chat_broadcast.c epoll_utils.c util.c
	$(CC) -o chatd-stdin $^ $(CFLAGS)

# repo init: load submodule dependencies
init:
	git submodule init
	git submodule update

clean:
	rm -f *.o chatd chat-cli exploit chat-fuzz chatd-stdin

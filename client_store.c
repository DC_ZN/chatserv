#include "client_store.h"
#include "broadcast_store.h"
#include "epoll_utils.h"
#include "util.h"
#include <stdio.h>
#include <unistd.h>

void hashtable_client_add(struct client_info** p_hashtable, int fd, uint32_t events, int epfd) {
    struct client_info* ev;

    HASH_FIND_INT(*p_hashtable, &(fd), ev);

    if (ev == NULL) {
        ev = malloc(sizeof(struct client_info));
        memset(ev, 0, sizeof(struct client_info));
        ev->fd = fd;
        HASH_ADD_INT(*p_hashtable, fd, ev);
    }
    ev->requested_events = events;
    epoll_ctl_add(epfd, fd, events);
}

void hashtable_client_update_events(struct client_info** p_hashtable, const struct epoll_event* event) {
    struct client_info* ev;

    HASH_FIND_INT(*p_hashtable, &(event->data.fd), ev);

    if (ev != NULL) {
        ev->active_events |= event->events;
        ev->requested_events &= ~event->events;
    }
}


// private
static void hashtable_client_free(struct client_info** p_hashtable, int fd) {
    struct client_info* ev;

    HASH_FIND_INT(*p_hashtable, &fd, ev);

    if (ev != NULL) {
        logd1("freeing the client");
        HASH_DEL(*p_hashtable, ev);
        free(ev->incoming_data);
        free(ev->outgoing_data);
        free(ev->username);
        chat_broadcast_unref(ev->current_broadcast);
        free(ev);
    }
}

void hashtable_client_schedule_disconnect(struct client_info** p_hashtable, int fd) {
    struct client_info* ev;

    HASH_FIND_INT(*p_hashtable, &fd, ev);

    if (ev != NULL) {
    logd("scheduling disconnect for fd=%d", fd);
         ev->client_state = CL_DISCONNECT;
    }
}

void hashtable_client_close(struct client_info** p_hashtable, int fd, int epfd) {
    logd1("closing the client");
    epoll_ctl(epfd, EPOLL_CTL_DEL, fd, NULL);
    close(fd);
    hashtable_client_free(p_hashtable, fd);
    printf("connection closed for fd=%d\n", fd);
}

#include "proto_inhat.h"
#include "epoll_utils.h"
#include "util.h"

#include <stdio.h>
#include <sys/socket.h>
#include <sys/epoll.h>
#include <netdb.h>
#include <stdlib.h>
#include <string.h>
#include <errno.h>
#include <unistd.h>

#define DEFAULT_PORT 4000
#define BUF_SIZE 4096

void write_chat_msg(int sockfd, char *msg, size_t msg_len);
int parse_chat_msg(uint8_t* data, int len);
void handle_broadcast(uint8_t *data, int len);

char *g_my_nick = NULL;

int main(int argc, char* argv[]) {

    if (argc < 3) {
        fprintf(stderr, "Usage: %s host port\n", argv[0]);
        exit(EXIT_FAILURE);
    }

    printf("Connecting to chat server...");
    fflush(stdout);

    struct addrinfo hints, *result;
    memset(&hints, 0, sizeof(struct addrinfo));
    hints.ai_family = AF_UNSPEC;    // Allow IPv4 or IPv6
    hints.ai_socktype = SOCK_STREAM;
    hints.ai_flags = 0;
    hints.ai_protocol = IPPROTO_TCP;

    int s = getaddrinfo(argv[1], argv[2], &hints, &result);
    if (s != 0) {
        fprintf(stderr, "getaddrinfo: %s\n", gai_strerror(s));
        exit(EXIT_FAILURE);
    }

    // getaddrinfo() returns a list of address structures.
    // Try each address until we successfully connect(2).
    // If socket(2) (or connect(2)) fails, we (close the socket
    // and) try the next address.
    int sockfd = -1;
    struct addrinfo *rp;
    for (rp = result; rp != NULL; rp = rp->ai_next) {
        sockfd = socket(rp->ai_family, rp->ai_socktype, rp->ai_protocol);
        if (sockfd == -1) {
            perror("socket");
            continue;
        }

        if (connect(sockfd, rp->ai_addr, rp->ai_addrlen) != -1) {
            break; // Success
        }
        //perror("connect");
        close(sockfd);
    }

    if (rp == NULL) {
        fprintf(stderr, "[error]: cannot connect to host");
        exit(EXIT_FAILURE);
    }

    printf("success.\nEnter your nick: ");

    size_t nick_len = getline(&g_my_nick, &nick_len, stdin);
    g_my_nick[nick_len - 1] = '\0'; // remove trailing '\n'

    printf("Logging in...");
    fflush(stdout);

    uint8_t buf[BUF_SIZE];
    int pos = 0;
    pos = inhat_write_data(buf, pos, (uint8_t *) PROTO_MAGIC, PROTO_MAGIC_LEN);
    pos = inhat_write_uint16(buf, pos, PROTO_VERSION);
    pos = inhat_write_msg(buf, pos, PROTO_MSG_LOGIN, (uint8_t *)g_my_nick, nick_len - 1);

    write(sockfd, buf, pos);

    uint8_t *data = buf;
    size_t len = sizeof(buf);
    for (;;) {
        int n = read(sockfd, data, len);
        if (n <= 0) {
            fprintf(stderr, "\n[error] Failed to read from server, exiting\n");
            perror("read");
            exit(EXIT_FAILURE);
        }
        if (n < inhat_sizeof_header()) {
            data += n;
            len -= n;
            continue;
        }
        break;
    }

    data = buf;
    len = sizeof(buf);
    uint32_t message_len;
    int nbytes = inhat_read_uint32(data, len, &message_len);
    //logd("Message len: %d", message_len);
    data += nbytes;
    len -= nbytes;
    if (message_len != inhat_sizeof_header() - sizeof(uint32_t)) {
        fprintf(stderr, "\n[error] Unexpected message received from server, exiting\n");
        exit(EXIT_FAILURE);
    }

    uint32_t checksum;
    nbytes = inhat_read_uint32(data, len, &checksum);
    data += nbytes;
    len -= nbytes;
    message_len -= nbytes;
    if (!inhat_validate_checksum(data, message_len, checksum)) {
        fprintf(stderr, "\n[error] Received message with invalid checksum, exiting\n");
        exit(EXIT_FAILURE);
    }

    uint16_t tag;
    nbytes = inhat_read_uint16(data, len, &tag);
    if (tag == PROTO_MSG_VERSION_MISMATCH) {
        fprintf(stderr, "\n[error] Server says version mismatch, exiting\n");
        exit(EXIT_FAILURE);
    } else if (tag == PROTO_MSG_ACK) {
        printf("success. Say hello to the chat!\n> ");
        fflush(stdout);
    } else {
        fprintf(stderr, "\n[error] Unexpected tag received from server, exiting\n");
        exit(EXIT_FAILURE);
    }

    setnonblocking(sockfd);

    const uint32_t BASE_EPOLL_MASK = EPOLLRDHUP | EPOLLHUP | EPOLLET | EPOLLONESHOT;
    int epfd = epoll_create1(0);
    epoll_ctl_add(epfd, sockfd, EPOLLIN | BASE_EPOLL_MASK);
    epoll_ctl_add(epfd, 0, EPOLLIN | BASE_EPOLL_MASK);

    struct epoll_event events[2];
    for (;;) {
        int nfds = epoll_wait(epfd, events, 2, -1);

        if (nfds == -1) {
            if (errno != EINTR) {
                perror("epoll_wait");
                exit(EXIT_FAILURE);
            }
        }

        for (int i = 0; i < nfds; i++) {
            uint32_t new_mask = BASE_EPOLL_MASK;
            if (events[i].data.fd == 0) {
                char *msg = NULL;
                size_t msg_len = getline(&msg, &msg_len, stdin);
                write_chat_msg(sockfd, msg, msg_len);
                new_mask |= EPOLLIN;
            } else {
                if (events[i].events & EPOLLIN) {
                    for (;;) {
                        ssize_t n = read(events[i].data.fd, buf, BUF_SIZE);
                        if (n <= 0) {
                            if (errno == EAGAIN || errno == EWOULDBLOCK) {
                                new_mask |= EPOLLIN;
                                break;
                            } else {
                                perror("reading");
                                exit(EXIT_FAILURE);
                            }
                        }
                        parse_chat_msg(buf, n);
                    }
                    printf("> ");
                    fflush(stdout);
                }
                if (events[i].events & (EPOLLHUP | EPOLLRDHUP)) {
                    printf("Connection to server lost!\n");
                    exit(EXIT_FAILURE);
                }
            }

            if (new_mask != BASE_EPOLL_MASK) {
                epoll_ctl_mod(epfd, events[i].data.fd, new_mask);
            }
        }
    }
}

void write_chat_msg(int sockfd, char* msg, size_t msg_len) {
    size_t out_len = inhat_sizeof_header() + msg_len;
    uint8_t *out = malloc(out_len);
    inhat_write_msg(out, 0, PROTO_MSG_BROADCAST, (uint8_t *)msg, msg_len);
    int nbytes = write(sockfd, out, out_len);
    if (nbytes < 0) {
        perror("writing");
    } else if (nbytes < out_len) {
        logd("Failed to write out_len=%zu, only nbytes=%d", out_len, nbytes);
    }
    free(out);
}

int parse_chat_msg(uint8_t *data, int len) {
    int nbytes;
    uint32_t message_len;
    if ((nbytes = inhat_read_uint32(data, len, &message_len)) < 0) {
        logd1("Not enough data to read message len");
        return 0;
    }
    if (message_len >= PROTO_MAX_MESSAGE_LEN) {
        logd1("Invalid len provided by server!");
        return 0;
    }
    data += nbytes;
    len -= nbytes;
    uint32_t full_len = message_len + sizeof(uint32_t);

    if (message_len > len) {
        logd("Not enough data to parse the message: available=%d, need message_len=%d",
             len, message_len);
        return 0;
    }

    uint32_t checksum;
    nbytes = inhat_read_uint32(data, len, &checksum);
    data += nbytes;
    len -= nbytes;
    message_len -= nbytes;

    if (!inhat_validate_checksum(data, message_len, checksum)) {
        logd("Invalid checksum %d. Discard the message", checksum);
        return full_len;
    }

    uint16_t tag;
    nbytes = inhat_read_uint16(data, len, &tag);
    data += nbytes;
    len -= nbytes;
    message_len -= nbytes;

    if (tag == PROTO_MSG_BROADCAST) {
        handle_broadcast(data, message_len);
    } else {
        logd("Received message tag=%d is not supported", tag);
    }
    return full_len;
}


void handle_broadcast(uint8_t* data, int len) {
    uint8_t uname_len;
    int nbytes = inhat_read_uint8(data, len, &uname_len);
    if (nbytes <= 0) {
        logd1("Couldn't read username len");
        return;
    }
    data += nbytes;
    len -= nbytes;
    if (uname_len > len) {
        logd("Invalid username len: %d  > %d", uname_len, len);
        return;
    }

    char *uname = malloc(uname_len + 1);
    nbytes = inhat_read_data(data, len, (uint8_t *)uname, uname_len);
    if (nbytes <= 0) {
        logd1("Couldn't read username");
        return;
    }
    data += nbytes;
    len -= nbytes;
    uname[uname_len] = '\0';

    uint32_t text_len;
    nbytes = inhat_read_uint32(data, len, &text_len);
    if (nbytes <= 0) {
        logd1("Couldn't read text len");
        return;
    }
    data += nbytes;
    len -= nbytes;
    if (text_len > len) {
        logd("Invalid text len: %d  > %d", text_len, len);
        return;
    }

    char *text = malloc(text_len + 1);
    nbytes = inhat_read_data(data, len, (uint8_t *)text, text_len);
    if (nbytes <= 0) {
        logd1("Couldn't read text");
        return;
    }
    data += nbytes;
    len -= nbytes;
    text[text_len] = '\0';

    if (strcmp(g_my_nick, uname) != 0) {
        printf("\b\b< [%s]: %s", uname, text);
    }
    free(uname);
    free(text);
}

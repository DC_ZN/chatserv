FROM alpine:3.8
RUN apk add --no-cache gcc musl-dev make
RUN adduser -S -h /home/chatd -s /bin/sh chatd
ADD ./ /service/
WORKDIR /service
RUN set -x && \
  make chatd && \
  chmod +x chatd
USER chatd
EXPOSE 4000
CMD ./chatd

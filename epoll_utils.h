#pragma once
#include <stdint.h>

// Register events of fd to epfd; returns -1 if failed
int epoll_ctl_add(int epfd, int fd, uint32_t events);

// Update events for fd in epfd; returns -1 if failed
int epoll_ctl_mod(int epfd, int fd, uint32_t events);

// Enable non-blocking IO on the socket
int setnonblocking(int sockfd);

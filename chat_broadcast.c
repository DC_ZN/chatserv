#include "chat_broadcast.h"
#include <stdlib.h>
#include <string.h>

chat_broadcast_t* chat_broadcast_alloc(char* from, uint8_t* data, int len) {
    chat_broadcast_t* el = malloc(sizeof(chat_broadcast_t));
    memset(el, 0, sizeof(chat_broadcast_t));
    el->from = malloc(strlen(from) + 1);
    strcpy(el->from, from);
    el->text = malloc(len + 1);
    memcpy(el->text, data, len);
    el->text[len] = '\0'; // TODO: escape all char
    return el;
}

void chat_broadcast_free(chat_broadcast_t* el) {
    if (el) {
        free(el->from);
        free(el->text);
        free(el);
    }
}

void chat_broadcast_ref(chat_broadcast_t *el) {
    if (el) {
        el->refcounter++;
    }
}

void chat_broadcast_unref(chat_broadcast_t *el) {
    if (el) {
        el->refcounter--;
    }
}

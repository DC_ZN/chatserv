#include "chat_broadcast.h"
#include "util.h"
#include "third_party/uthash/include/utlist.h"
#include <stdio.h>
#include <stdlib.h>
#include <string.h>

static chat_broadcast_t* s_chat_broadcasts = NULL;
static int s_chat_broadcasts_counter = 0;

void broadcast_store_add(chat_broadcast_t* el) {
    DL_APPEND(s_chat_broadcasts, el);
    s_chat_broadcasts_counter++;
}

void broadcast_store_del(chat_broadcast_t* el) {
    DL_DELETE(s_chat_broadcasts, el);
    s_chat_broadcasts_counter--;
}

bool broadcast_store_has_next(chat_broadcast_t* el) {
    if (!el) {
        el = s_chat_broadcasts;
    } else {
        el = el->next;
    }
    return el != NULL;
}

chat_broadcast_t* broadcast_store_acquire_next(chat_broadcast_t* el) {
    chat_broadcast_t* next;
    if (!el) { // new client
        next = s_chat_broadcasts;
    } else {
        chat_broadcast_unref(el);
        next = el->next;
    }

    chat_broadcast_ref(next);

    return next;
}

#define BROADCAST_CLEANUP_START_LIMIT 100
#define BROADCAST_CLEANUP_STOP_LIMIT 80
void broadcast_storage_gc() {
    logd("Attempt garbadge collection, number of objs: %d", s_chat_broadcasts_counter);
    if (s_chat_broadcasts_counter > BROADCAST_CLEANUP_START_LIMIT) {
        logd1("Garbadge collection started");
        int count = 0;
        // we have too many broadcasts stored
        // remove the ones that have been sent to existing clients
        chat_broadcast_t* el, *tmp;
        DL_FOREACH_SAFE(s_chat_broadcasts, el, tmp) {
            if (el->refcounter <= 0) {
                count++;
                logd("GC: %s, count: %d, its refcounter: %d", el->text, count, el->refcounter);
                broadcast_store_del(el);
                chat_broadcast_free(el);
            } else {
                // stop on first referenced element
                logd("GC stopped on first referenced element at %s, count: %d, its refcounter: %d",
                     el->text, count, el->refcounter);
                break;
            }
            if (s_chat_broadcasts_counter <= BROADCAST_CLEANUP_STOP_LIMIT) {
                logd("GC stopped on stop limit at %s, count: %d, its refcounter: %d", el->text, count, el->refcounter);
                break;
            }
        }
    }
}

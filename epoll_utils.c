#include "epoll_utils.h"
#include "sys/epoll.h"
#include <stdio.h>
#include <fcntl.h>


int epoll_ctl_add(int epfd, int fd, uint32_t events) {
    struct epoll_event ev;
    ev.events = events;
    ev.data.fd = fd;
    if (epoll_ctl(epfd, EPOLL_CTL_ADD, fd, &ev) == -1) {
        perror("epoll_ctl_add");
        return -1;
    }
    return 0;
}

int epoll_ctl_mod(int epfd, int fd, uint32_t events) {
    struct epoll_event ev;
    ev.events = events;
    ev.data.fd = fd;
    if (epoll_ctl(epfd, EPOLL_CTL_MOD, fd, &ev) == -1) {
        perror("epoll_ctl_mod");
        return -1;
    }
    return 0;
}

int setnonblocking(int sockfd) {
    int flags = fcntl(sockfd, F_GETFD, 0);
    if (flags == -1) {
        perror("setnonblocking getfd");
        return -1;
    }

    if (fcntl(sockfd, F_SETFL, flags | O_NONBLOCK) == -1) {
        perror("setnonblocking setfd");
        return -1;
    }
    return 0;
}

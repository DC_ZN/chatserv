#pragma once

#include <stdbool.h>

void broadcast_store_add(chat_broadcast_t* el);
void broadcast_store_del(chat_broadcast_t* el);

bool broadcast_store_has_next(chat_broadcast_t* el);

chat_broadcast_t* broadcast_store_acquire_next(chat_broadcast_t* el);

void broadcast_storage_gc();

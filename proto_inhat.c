#include "proto_inhat.h"
#include "util.h"
#include <string.h>
#include <stdio.h>

const char PROTO_MAGIC[PROTO_MAGIC_LEN] = {'I', 'N', 'H', 'A', 'T'};

int inhat_check_magic(uint8_t* data, int len) {
    if (len < PROTO_MAGIC_LEN) {
        return -1;
    } else if (strncmp(PROTO_MAGIC, (char*)data, PROTO_MAGIC_LEN)) {
        return -1;
    } else {
        return PROTO_MAGIC_LEN;
    }
}

bool inhat_check_version(uint16_t version) {
    return ((version >> 8) == PROTO_VERSION_MAJOR) && ((version & 0xff) <= PROTO_VERSION_MINOR);
}

int inhat_sizeof_header() {
    // length + checksum + tag
    return sizeof(uint32_t) + sizeof(uint32_t) + sizeof(uint16_t);
}


int inhat_read_uint8(uint8_t* data, int len, uint8_t* out) {
    if (len < sizeof(uint8_t)) {
        return -1;
    }
    *out = data[0];
    return sizeof(uint8_t);
}

int inhat_read_uint16(uint8_t* data, int len, uint16_t* out) {
    if (len < sizeof(uint16_t)) {
        return -1;
    }
    //logd("%x %x", data[0], data[1]);
    *out = ((uint16_t)data[0] << 8) | data[1];
    return sizeof(uint16_t);
}

int inhat_read_uint32(uint8_t* data, int len, uint32_t* out) {
    if (len < sizeof(uint32_t)) {
        return -1;
    }
    *out = ((uint32_t)data[0] << 24) | ((uint32_t)data[1] << 16)
           | ((uint32_t)data[2] << 8)  | data[3];
    return sizeof(uint32_t);
}

int inhat_read_data(uint8_t* data, int len, uint8_t* out, int outlen) {
    if (len < outlen) {
        return -1;
    }
    memcpy(out, data, outlen);
    return outlen;
}

int inhat_write_uint8(uint8_t* to, int pos, uint8_t d) {
    to[pos++] = d;
    return pos;
}

int inhat_write_uint16(uint8_t* to, int pos, uint16_t d) {
    to[pos++] = d >> 8;
    to[pos++] = d & 0xff;
    return pos;
}

int inhat_write_uint32(uint8_t* to, int pos, uint32_t d) {
    to[pos++] = d >> 24;
    to[pos++] = (d >> 16) & 0xff;
    to[pos++] = (d >> 8) & 0xff;
    to[pos++] = d & 0xff;
    return pos;
}

int inhat_write_data(uint8_t* to, int pos, const uint8_t* s, int len) {
    memcpy(to + pos, s, len);
    return pos + len;
}

int inhat_write_msg(uint8_t* to, int pos, inhat_tag_t tag, const uint8_t* data, int len) {
    int full_len = sizeof(uint32_t) + sizeof(uint16_t) + len; // checksum + tag + data
    pos = inhat_write_uint32(to, pos, full_len);
    uint32_t checksum = inhat_calc_checksum(tag, data, len);
    pos = inhat_write_uint32(to, pos, checksum);
    pos = inhat_write_uint16(to, pos, tag);
    pos = inhat_write_data(to, pos, data, len);
    return pos;
}

uint32_t inhat_calc_checksum(uint16_t tag, const uint8_t* data, int len) {
    uint32_t sum = (tag >> 8) + (tag & 0xff);
    for (int i = 0; i < len; i++) {
        sum += data[i];
    }
    return sum;
}

bool inhat_validate_checksum(const uint8_t* data, int len, uint32_t checksum) {
    uint32_t sum = 0;
    for (int i = 0; i < len; i++) {
        sum += data[i];
    }
    return sum == checksum;
}

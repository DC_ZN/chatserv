/**
 * Public domain. Author: wsnark
 *
 * server.c -- a vulnerable stream socket server
 */

#include "proto_inhat.h"
#include "client_store.h"
#include "broadcast_store.h"
#include "epoll_utils.h"
#include "util.h"

#include <sys/types.h>
#include <sys/socket.h>
#include <netdb.h>
#include <arpa/inet.h>
#include <stdio.h>
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <string.h>
#include <sys/epoll.h>
#include <fcntl.h>
#include <sys/stat.h>
#include <stdbool.h>

#define PORT "4000"  // the port users will be connecting to
#define BACKLOG 10     // how many pending connections queue will hold
#define MAX_EPOLL_EVENTS 32 // number of epoll events to handle at once
#define READ_BUF_SIZE 4096 // read buffer size

const uint32_t BASE_EPOLL_MASK = EPOLLRDHUP | EPOLLHUP | EPOLLET | EPOLLONESHOT;

// Get address from IPv4 or IPv6 sockaddr
void* get_in_addr(struct sockaddr* sa) {
    if (sa->sa_family == AF_INET) {
        return &(((struct sockaddr_in*)sa)->sin_addr);
    }

    return &(((struct sockaddr_in6*)sa)->sin6_addr);
}

// Get port from IPv4 or IPv6 sockaddr
in_port_t get_in_port(struct sockaddr* sa) {
    if (sa->sa_family == AF_INET) {
        return ((struct sockaddr_in*)sa)->sin_port;
    }

    return ((struct sockaddr_in6*)sa)->sin6_port;
}

void add_proto_msg_with_data(struct client_info* ev, uint16_t tag, const uint8_t *data, size_t len) {
    size_t pos = ev->outgoing_len;
    ev->outgoing_len += inhat_sizeof_header() + len;
    ev->outgoing_data = realloc(ev->outgoing_data, ev->outgoing_len);
    inhat_write_msg(ev->outgoing_data, pos, tag, data, len);
}

void add_proto_msg_ack(struct client_info* ev) {
    add_proto_msg_with_data(ev, PROTO_MSG_ACK, NULL, 0);
}

void add_proto_msg_nack(struct client_info* ev, const char *reason) {
    size_t len = strlen(reason);
    add_proto_msg_with_data(ev, PROTO_MSG_NACK, (const uint8_t *)reason, len);
}

void add_proto_msg_version_mismatch(struct client_info* ev) {
    add_proto_msg_with_data(ev, PROTO_MSG_VERSION_MISMATCH, NULL, 0);
}

bool need_to_broadcast(struct client_info* ev) {
    bool need = ev->client_state == CL_LOGGED_IN && broadcast_store_has_next(ev->current_broadcast);
    logd("Need to broadcast: %d", need);
    return need;
}

void add_proto_msg_broadcast(struct client_info* ev) {
    if (!need_to_broadcast(ev)) {
        return;
    }

    ev->current_broadcast = broadcast_store_acquire_next(ev->current_broadcast);

    // TODO: store string lengths
    uint8_t ulen = strlen(ev->current_broadcast->from);
    uint32_t tlen = strlen(ev->current_broadcast->text);
    size_t data_len = sizeof(uint8_t) + ulen + sizeof(uint32_t) + tlen;
    ev->outgoing_len = inhat_sizeof_header() + data_len;
    ev->outgoing_data = malloc(ev->outgoing_len);

    int pos = 0;
    // len doesn't count itself
    pos = inhat_write_uint32(ev->outgoing_data, pos, ev->outgoing_len - sizeof(uint32_t));
    int checksum_pos = pos;
    pos += sizeof(uint32_t);
    pos = inhat_write_uint16(ev->outgoing_data, pos, PROTO_MSG_BROADCAST);
    pos = inhat_write_uint8(ev->outgoing_data, pos, ulen);
    pos = inhat_write_data(ev->outgoing_data, pos, (uint8_t*)ev->current_broadcast->from, ulen);

    pos = inhat_write_uint32(ev->outgoing_data, pos, tlen);
    pos = inhat_write_data(ev->outgoing_data, pos, (uint8_t*)ev->current_broadcast->text, tlen);

    uint32_t checksum = inhat_calc_checksum(PROTO_MSG_BROADCAST, ev->outgoing_data + checksum_pos + sizeof(uint32_t) + sizeof(uint16_t), data_len);
    inhat_write_uint32(ev->outgoing_data, checksum_pos, checksum);
}

#define AVA_DIR_ROOT "/avatars/"
bool save_avatar(char *username, uint8_t *data, uint32_t len) {
    bool ret = false;
    char *filename = NULL;
    uint8_t *content = NULL;
    char * ava = NULL;

    uint8_t filename_len;
    int nbytes = inhat_read_uint8(data, len, &filename_len);
    if (nbytes <= 0) {
        logd1("Couldn't read filename_len");
        goto cleanup;
    }
    data += nbytes;
    len -= nbytes;
    if (filename_len > len) {
        logd("Invalid filename len: %d  > %d", filename_len, len);
        goto cleanup;
    }

    filename = malloc(filename_len + 1);
    nbytes = inhat_read_data(data, len, (uint8_t *)filename, filename_len);
    if (nbytes <= 0) {
        logd1("Couldn't read filename");
        goto cleanup;
    }
    data += nbytes;
    len -= nbytes;
    filename[filename_len] = '\0';

    uint32_t content_len;
    nbytes = inhat_read_uint32(data, len, &content_len);
    if (nbytes <= 0) {
        logd1("Couldn't read content_len");
        goto cleanup;
    }
    data += nbytes;
    len -= nbytes;
    if (content_len > len) {
        logd("Invalid content_len: %d  > %d", content_len, len);
        goto cleanup;
    }

    content = malloc(content_len);
    nbytes = inhat_read_data(data, len, content, content_len);
    if (nbytes <= 0) {
        logd1("Couldn't read content");
        goto cleanup;
    }
    data += nbytes;
    len -= nbytes;

    int ava_dir_len = strlen(AVA_DIR_ROOT) + strlen(username);
    ava = malloc(ava_dir_len + 1 + filename_len + 1); // 1 - for /, last 1 for \0
    sprintf(ava, "%s", AVA_DIR_ROOT);
    sprintf(ava + strlen(AVA_DIR_ROOT), "%s", username);
    struct stat st;

    if (stat(ava, &st) != 0) {
        perror("stat");
        logd("Creating ava_dir: %s", ava);
        if (mkdir(ava, S_IRWXU | S_IRWXG | S_IROTH | S_IXOTH) != 0) {
            perror("creating ava dir failed");
            goto cleanup;
        }
    }

    sprintf(ava + ava_dir_len, "/%s", filename);
    FILE *f = fopen(ava, "wb");
    if (!f) {
        logd("Failed to fopen ava file: %s", ava);
        goto cleanup;
    }

    size_t written = fwrite(content, sizeof(uint8_t), content_len, f);
    logd("Writing ava: %s, requested to write %d, written %zu", ava, content_len, written);
    fclose(f);
    ret = true;

cleanup:
    free(filename);
    free(content);
    free(ava);
    return ret;
}

// Returns true if msg is successfully handled, false if not
bool handle_message(struct client_info* ev, uint16_t tag, uint8_t* data, uint32_t message_len) {
    switch (tag) {
    case PROTO_MSG_LOGIN:
        if (ev->client_state != CL_RECOGNIZED) {
            logd("Unexpected PROTO_MSG_CHELLO in client_state=%d => disconnect fd=%d",
                 ev->client_state, ev->fd);
            ev->client_state = CL_DISCONNECT;
            return false;
        }
        if (message_len == 0 || message_len > PROTO_MAX_USERNAME_LEN) {
            logd("Empty or too long username isn't allowed => disconnect fd=%d",
                 ev->fd);
            ev->client_state = CL_DISCONNECT;
            return false;
        }
        ev->username = malloc(message_len + 1);
        memcpy(ev->username, data, message_len);
        ev->username[message_len] = '\0';
        // FIXME: escape username, e.g. it may have \0 inside!
        printf("[+] %s logged in, fd=%d", ev->username, ev->fd);
        ev->client_state = CL_LOGGED_IN;

        add_proto_msg_ack(ev);
        break;

    case PROTO_MSG_BYE:
        if (ev->client_state != CL_LOGGED_IN) {
            logd("Unexpected PROTO_MSG_BYE in client_state=%d => disconnect fd=%d",
                 ev->client_state, ev->fd);
            ev->client_state = CL_DISCONNECT;
            return false;
        }
        printf("[-] %s logged out, fd=%d", ev->username, ev->fd);
        ev->client_state = CL_DISCONNECT;
        break;

    case PROTO_MSG_BROADCAST:
        if (ev->client_state != CL_LOGGED_IN) {
            logd("Unexpected PROTO_MSG_BROADCAST in client_state=%d => disconnect fd=%d",
                 ev->client_state, ev->fd);
            ev->client_state = CL_DISCONNECT;
            return false;
        }
        chat_broadcast_t* br = chat_broadcast_alloc(ev->username, data, message_len);
        printf("[b] %s, fd=%d says: %s", ev->username, ev->fd, br->text);
        broadcast_store_add(br);
        break;
    case PROTO_MSG_UPLOAD_AVATAR:
        if (ev->client_state != CL_LOGGED_IN) {
            logd("Unexpected PROTO_MSG_BROADCAST in client_state=%d => disconnect fd=%d",
                 ev->client_state, ev->fd);
            ev->client_state = CL_DISCONNECT;
            return false;
        }
        printf("[ava] %s, fd=%d uploads avatar", ev->username, ev->fd);
        if (save_avatar(ev->username, data, message_len)) {
            add_proto_msg_ack(ev);
        } else {
            add_proto_msg_nack(ev, "upload failed");
        }
        break;

    default:
        logd("Unexpected message tag %d in client_state=%d => disconnect fd=%d",
             tag, ev->client_state, ev->fd);
        ev->client_state = CL_DISCONNECT;
        return false;
    }
    return true;
}

// Returns length of parsed message
int proto_parse_message(struct client_info* ev, uint8_t* data, int len) {
    int nbytes;
    uint32_t message_len;
    if ((nbytes = inhat_read_uint32(data, len, &message_len)) < 0) {
        logd("Not enough data to read message len, client fd=%d", ev->fd);
        return 0;
    }
    if (message_len >= PROTO_MAX_MESSAGE_LEN) {
        logd("Invalid len %d provided by client fd=%d!", message_len, ev->fd);

        // To increase afl coverage level, invalid length converted to accepted one
        message_len = PROTO_MAX_MESSAGE_LEN;
        // ev->client_state = CL_DISCONNECT;
        // return 0;
    }
    data += nbytes;
    len -= nbytes;
    uint32_t full_len = message_len + sizeof(uint32_t);

    if (message_len > len) {
        logd("Not enough data to parse the message: available=%d, need message_len=%d, client fd=%d",
             len, message_len, ev->fd);
        return 0;
    }

    uint32_t checksum;
    nbytes = inhat_read_uint32(data, len, &checksum);
    data += nbytes;
    len -= nbytes;
    message_len -= nbytes;

    if (!inhat_validate_checksum(data, message_len, checksum)) {
        // disable checksum validation for afl
        //logd("Invalid checksum %d. Discard the message, client fd=%d", checksum, ev->fd);
        //return full_len;
    }

    uint16_t tag;
    nbytes = inhat_read_uint16(data, len, &tag);
    data += nbytes;
    len -= nbytes;
    message_len -= nbytes;
    logd("Have read tag=%d, client fd=%d", tag, ev->fd);

    handle_message(ev, tag, data, message_len);
    return full_len;
}

// Returns number of remaining bytes
int proto_parse(struct client_info* ev) {
    int nbytes;
    uint8_t* data = ev->incoming_data;
    int len = ev->incoming_len;
    if (ev->client_state == CL_UNKNOWN) {
        if ((nbytes = inhat_check_magic(data, len)) < 0) {
            logd("Protocol magic not recognized for client fd=%d", ev->fd);
            ev->client_state = CL_DISCONNECT;
            return 0;
        }
        data += nbytes;
        len -= nbytes;

        uint16_t version;
        if ((nbytes = inhat_read_uint16(data, len, &version)) < 0) {
            return ev->incoming_len; // wait for more data
        }
        data += nbytes;
        len -= nbytes;

        if (!inhat_check_version(version)) {
            logd("Protocol version mismatch: %d for client fd=%d", version, ev->fd);
            add_proto_msg_version_mismatch(ev);
            ev->client_state = CL_DISCONNECTING;
            return 0;
        }

        logd("Client fd=%d recognized", ev->fd);
        ev->client_state = CL_RECOGNIZED;
    }

    while ((nbytes = proto_parse_message(ev, data, len)) > 0) {
        data += nbytes;
        len -= nbytes;
    }

    return len;
}

#ifdef LOG_TO_FILE
FILE *logfile;
#endif

int main(void) {
    #ifdef LOG_TO_FILE
    logfile = fopen("chatserv.log", "w");
    #endif

    int epfd = epoll_create1(0);

    uint8_t buf[READ_BUF_SIZE];

    struct client_info* hashtable = NULL;

    struct epoll_event events[MAX_EPOLL_EVENTS];
    int timeout = 1;

    setnonblocking(0);
    hashtable_client_add(&hashtable, 0, BASE_EPOLL_MASK | EPOLLIN, epfd);

    bool first_read = true;
    const uint8_t successfull_login[] = { 0x49, 0x4e, 0x48, 0x41, 0x54, // INHAT
        0x01, 0x00, // Version 1.0
        // Login msg:
        0x00, 0x00, 0x00, 0x0b, // len = 0xb
        0x00, 0x00, 0x01, 0xf0, // checksum = 0x1f0
        0x00, 0x00, // tag = 0x0
        0x46, 0x65, 0x64, 0x6f, 0x72 }; // username = "Fedor"

    for (;;) {
        logd("Starting epoll_wait, timeout=%d", timeout);
        int nfds = epoll_wait(epfd, events, MAX_EPOLL_EVENTS, timeout);
        if (nfds == -1) {
            if (errno != EINTR) {
                perror("epoll_wait");
                exit(EXIT_FAILURE);
            }
        }

        for (int i = 0; i < nfds; i++) {
            if (events[i].events & (EPOLLIN | EPOLLOUT)) {
                logd("ready for IO at fd=%d", events[i].data.fd);
                hashtable_client_update_events(&hashtable, &events[i]);
            }

            if (events[i].events & (EPOLLRDHUP | EPOLLHUP)) {
                // disable early disconnect; though not needed for afl, needed for 'cat'/radamsa
                //hashtable_client_schedule_disconnect(&hashtable, events[i].data.fd);
                continue;
            }

        }

        bool nothing_to_do = true; // nothing to do with existing fds, i.e. need to wait
        logd1("Handling actions scheduled on all fds");
        struct client_info* ev, *tmp;
        HASH_ITER(hh, hashtable, ev, tmp) {
            if (ev->client_state == CL_DISCONNECTING) {
                // ignore incoming data for server-enforced disconnecting client
                ev->requested_events &= ~EPOLLIN;
                ev->active_events &= ~EPOLLIN;
            }

            // This is needed for afl, as it doesn't feed stdin in a normal manner...
            if (first_read) {
                ev->active_events |= EPOLLIN;
            }

            if (ev->active_events & EPOLLIN) {
                bool can_read = false;
                for (;;) {
                    logd("Starting read for client fd=%d", ev->fd);
                    ssize_t n = read(ev->fd, buf, READ_BUF_SIZE);
                    if (n <= 0) {
                        if (n == 0 || errno == EAGAIN || errno == EWOULDBLOCK) {
                            logd("EAGAIN for client fd=%d", ev->fd);
                            //break; // we have read everything up now, stop reading
                            exit(EXIT_SUCCESS);
                        } else if (errno == EINTR) {
                            continue; // read was interrupted, need to retry right now
                        } else {
                            perror("reading");
                            hashtable_client_close(&hashtable, ev->fd, epfd);
                            goto client_handle_end;
                            break;
                        }
                    } else if (n == READ_BUF_SIZE) {
                        can_read = true; // we have read full buf, so probably more is ready
                    }

                    logd_hex("Read buffer: ", buf, n);

                    if (first_read) {
                        ev->incoming_data = malloc(sizeof(successfull_login));
                        memcpy(ev->incoming_data, successfull_login, sizeof(successfull_login));
                        ev->incoming_len = sizeof(successfull_login);
                        first_read = false;
                    }

                    // should work for any ev->incoming_data (NULL, non-NULL)
                    ev->incoming_data = realloc(ev->incoming_data, ev->incoming_len + n);
                    memcpy(ev->incoming_data + ev->incoming_len, buf, n);
                    ev->incoming_len += n;

                    logd_hex("ev->incoming_data: ", ev->incoming_data, ev->incoming_len);
                    int remaining_len = proto_parse(ev);

                    if (ev->client_state == CL_DISCONNECT) {
                        hashtable_client_close(&hashtable, ev->fd, epfd);
                        goto client_handle_end;
                    }

                    if (remaining_len > 0) {
                        memmove(ev->incoming_data,
                                ev->incoming_data + ev->incoming_len - remaining_len,
                                remaining_len);
                        ev->incoming_len = remaining_len;
                        ev->incoming_data = realloc(ev->incoming_data, ev->incoming_len);
                    } else {
                        free(ev->incoming_data);
                        ev->incoming_data = NULL;
                        ev->incoming_len = 0;
                    }

                    break; // only 1 iter of this loop normally to avoid IO starvation
                }

                if (!can_read) {
                    logd("Exhausted input space for client fd=%d, request notification", ev->fd);
                    ev->active_events &= ~EPOLLIN; // stop reading via this handler
                    ev->requested_events |= EPOLLIN; // request notification when new data is available
                }

                // if we can read, then we have smth to do
                nothing_to_do = nothing_to_do && !can_read;
            }

            if (ev->active_events & EPOLLOUT) {
                bool can_write = false;
                bool need_write = false;

                if (!ev->outgoing_data) {
                    add_proto_msg_broadcast(ev);
                }

                if (ev->outgoing_data) {
                    for (;;) {
                        ssize_t n = write(ev->fd, ev->outgoing_data, ev->outgoing_len);
                        if (n <= 0) {
                            if (errno == EINTR) {
                                logd("Write was interrupted, need to retry right now, fd=%d",
                                     ev->fd);
                                continue;
                            } else if (errno == EAGAIN || errno == EWOULDBLOCK) {
                                logd("Filled the write buffer and need more, fd=%d", ev->fd);
                                need_write = true;
                                break;
                            } else {
                                perror("writing");
                                hashtable_client_close(&hashtable, ev->fd, epfd);
                                goto client_handle_end;
                                break;
                            }
                        } else if (n == ev->outgoing_len) {
                            logd("Have written all outgoing_data, can write more, fd=%d", ev->fd);
                            free(ev->outgoing_data);
                            ev->outgoing_data = NULL;
                            ev->outgoing_len = 0;

                            if (ev->client_state == CL_DISCONNECTING) {
                                hashtable_client_close(&hashtable, ev->fd, epfd);
                                goto client_handle_end;
                            }

                            can_write = true;
                            if (need_to_broadcast(ev)) {
                                need_write = true;
                            }
                        } else {
                            // have written part of the data
                            can_write = true;
                            need_write = true;
                            // TODO: improve efficiency - avoid these moves
                            memmove(ev->outgoing_data, ev->outgoing_data + n, n);
                            ev->outgoing_len -= n;
                            ev->outgoing_data = realloc(ev->outgoing_data, ev->outgoing_len);
                        }
                        break; // only 1 iter of this loop normally to avoid IO starvation
                    }
                }

                if (!can_write) {
                    logd("Exhausted output space for client fd=%d", ev->fd);
                    ev->active_events &= ~EPOLLOUT; // stop writing via this handler
                    if (need_write) {
                        logd("Still need to write, request notification, fd=%d", ev->fd);
                        ev->requested_events |= EPOLLOUT; // request writing
                    }
                }

                // for afl: ignore writing as work to do - it doesn't read...
                //nothing_to_do = nothing_to_do && !(can_write && need_write);

            } else { // not writing in this iteration
                if (ev->outgoing_data || need_to_broadcast(ev)) {
                    logd("Need to start writing, request notification, fd=%d", ev->fd);
                    ev->requested_events |= EPOLLOUT; // request writing
                }
            }

            // for afl: close connection when nothing to read, regardless of connection state
            //if (nothing_to_do && ev->client_state == CL_DISCONNECT) {
            if (nothing_to_do) {
                hashtable_client_close(&hashtable, ev->fd, epfd);
                goto client_handle_end;
            }

            logd("epoll_ctl_mod, EPOLLOUT: %d, EPOLLIN: %d, fd=%d",
                 (ev->requested_events & EPOLLOUT) != 0, (ev->requested_events & EPOLLIN) != 0, ev->fd);

            epoll_ctl_mod(epfd, ev->fd, ev->requested_events);
        }

        broadcast_storage_gc();
        timeout = nothing_to_do ? -1 : 0;
    }

client_handle_end:
    #ifdef LOG_TO_FILE
    fclose(logfile);
    #endif
    return 0;
}

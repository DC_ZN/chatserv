#pragma once

#include <stdint.h>
#include <stdbool.h>

#define PROTO_MAGIC_LEN 5
extern const char PROTO_MAGIC[];

#define PROTO_VERSION_MAJOR 0x1
#define PROTO_VERSION_MINOR 0x0
#define PROTO_VERSION ((PROTO_VERSION_MAJOR << 8) | PROTO_VERSION_MINOR)
#define PROTO_MAX_MESSAGE_LEN 1024
#define PROTO_MAX_USERNAME_LEN 32

typedef enum {
    PROTO_MSG_LOGIN,
    PROTO_MSG_ACK,
    PROTO_MSG_UPLOAD_AVATAR,
    PROTO_MSG_BROADCAST,
    PROTO_MSG_VERSION_MISMATCH,
    PROTO_MSG_NACK,
    PROTO_MSG_BYE
} inhat_tag_t;

// Returns number of bytes in correct magic, or -1 if it is not found
int inhat_check_magic(uint8_t *data, int len);

// Returns whether version is supported
bool inhat_check_version(uint16_t version);

int inhat_sizeof_header();

// Read functions return number of bytes read
int inhat_read_uint8(uint8_t *data, int len, uint8_t *out);
int inhat_read_uint16(uint8_t *data, int len, uint16_t *out);
int inhat_read_uint32(uint8_t *data, int len, uint32_t *out);
int inhat_read_data(uint8_t *data, int len, uint8_t *out, int outlen);


// Write functions return new position, i.e. pos + number of bytes written
int inhat_write_uint8(uint8_t *to, int pos, uint8_t d);
int inhat_write_uint16(uint8_t *to, int pos, uint16_t d);
int inhat_write_uint32(uint8_t *to, int pos, uint32_t d);
int inhat_write_data(uint8_t *to, int pos, const uint8_t *s, int len);
int inhat_write_msg(uint8_t *to, int pos, inhat_tag_t tag, const uint8_t *data, int len);

uint32_t inhat_calc_checksum(uint16_t tag, const uint8_t *data, int len);
bool inhat_validate_checksum(const uint8_t* data, int len, uint32_t checksum);
